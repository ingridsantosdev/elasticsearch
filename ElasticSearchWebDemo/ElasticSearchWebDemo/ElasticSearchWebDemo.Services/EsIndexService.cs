﻿using ElasticSearchWebDemo.Models;
using System;
using System.Threading.Tasks;

namespace ElasticSearchWebDemo.Services
{
    public class EsIndexService : IEsIndexService
    {
        private IConnection _connection;
        public EsIndexService(IConnection connection)
        {
            _connection = connection;
        }

        public async Task<Object> CreateEsIndexAsync(IndexRequest request) =>
            await _connection.ElasticClient.CreateIndexAsync(request.Name);

        public async Task<Object> DeleteEsIndexAsync(IndexRequest request) =>
            await _connection.ElasticClient.DeleteIndexAsync(request.Name);
    }

    public interface IEsIndexService
    {
        Task<Object> CreateEsIndexAsync(IndexRequest createIndexRequest);

        Task<Object> DeleteEsIndexAsync(IndexRequest request);
    }
}
