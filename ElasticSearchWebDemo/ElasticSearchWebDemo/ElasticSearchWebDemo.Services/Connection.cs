﻿using Nest;
using System;

namespace ElasticSearchWebDemo.Services
{
    public class Connection : IConnection
    {
        public ElasticClient ElasticClient { get; set; }

        public Connection()
        {
            // NEST Client
            var settings = new ConnectionSettings(new Uri("http://localhost:9200/"))
                .DefaultIndex("logging");

            ElasticClient = new ElasticClient(settings);
        }
    }

    public interface IConnection
    {
        ElasticClient ElasticClient { get; set; }
    }
}
