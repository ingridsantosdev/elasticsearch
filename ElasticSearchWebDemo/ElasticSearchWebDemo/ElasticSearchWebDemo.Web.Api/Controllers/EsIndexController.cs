﻿using System;
using System.Threading.Tasks;
using ElasticSearchWebDemo.Models;
using ElasticSearchWebDemo.Services;
using Microsoft.AspNetCore.Mvc;

namespace ElasticSearchWebDemo.Web.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/EsIndex")]
    public class EsIndexController : Controller
    {
        private readonly IEsIndexService _esIndexService;

        public EsIndexController(IEsIndexService esIndexService)
        {
            _esIndexService = esIndexService;
        }

        [HttpPost("Create")]
        public async Task<Object> Create([FromBody]IndexRequest indexRequest) =>
            await _esIndexService.CreateEsIndexAsync(indexRequest);

        [HttpPost("Delete")]
        public async Task<Object> Delete([FromBody]IndexRequest indexRequest) =>
            await _esIndexService.DeleteEsIndexAsync(indexRequest);
    }
}